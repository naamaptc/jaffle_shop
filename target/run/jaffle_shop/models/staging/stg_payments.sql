
  create or replace  view "VUFORIAUSAGEDB_SANDBOX".GOOGLE_SHEETS.stg_payments
  
   as (
    with source as (
    select * from google_sheets.RAW_PAYMENTS

),

renamed as (

    select
        id as payment_id,
        order_id,
        payment_method,

        -- `amount` is currently stored in cents, so we convert it to dollars
        amount / 100 as amount

    from source

)

select * from renamed
  );
