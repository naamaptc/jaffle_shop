
  create or replace  view "VUFORIAUSAGEDB_SANDBOX".GOOGLE_SHEETS.stg_customers
  
   as (
    with source as (
    select * from VUFORIAUSAGEDB_SANDBOX.google_sheets.raw_customers


),

renamed as (

    select
        id as customer_id,
        first_name,
        last_name

    from source

)

select * from renamed
  );
